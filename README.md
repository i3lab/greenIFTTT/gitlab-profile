# GreenIFTTT
This is the repository of _GreenIFTTT_ project, developed for the "Advanced User Interfaces" Course Project (prof. Franca Garzotto and tutor Mathyas Giudici) @ Politecnico di Milano, a.a. 2023/2024.

## Overview
The advent of Large Language Models (LLMs) in the field of Artificial Intelligence has revolutionized various applications, including text summarization, story and creative generation, and code development. These models, employing deep learning techniques and massive datasets, have become integral in understanding, generating, and predicting content.
While LLMs have found applications in diverse domains such as robotics, video games, and smart home automation, their integration into web applications for advising users on household sustainable practices remains unexplored. In this context, our research introduces a web-based conversational agent, named GreenIFTTT, powered by the GPT-4 model aimed to encourage individuals, particularly homeowners, to adopt environmentally conscious habits within their households.
The key features of GreenIFTTT include educating users on sustainable electricity consumption, reducing overall energy consumption, and simplifying routines's creation with smart technologies. The system focuses on creating routines - automation sequences within the home environment based on the sequential execution of specific activities triggered by various conditions.
The primary interaction paradigm is conversational, leveraging the capabilities of the LLM to assist users in locating and monitoring their smart appliances. The system also integrates data from connected sensors, providing users with real-time insights into their daily routines.

## How To
To run the project on your machine, you need to have **Node.js** installed on your machine, and to follow the instructions below:

1. create a `.env` file in the `backend/` folder containing the following parameters:
    - `DATABASE_URL` with the connection string to the database to be used by PrismaORM;
    - `TOKEN_KEY` with the secret key for JWT;
    - `OPENAI_KEY` with the token for the connection to the OpenAI API;
2. open the `frontend/src/main.js` file and change the following parameters:
    - [line 10] `axios.defaults.baseURL` to the URL of the backend server (by default [http://localhost:8080/api/v3](http://localhost:8080/api/v3));
    - [line 11] `WEBSOCKET_BASEURL` to the URL of the logger (WebSocket server, by default [ws://localhost:8081/](ws://localhost:8081/));
3. install the missing dependencies by running into a shell:
    - `cd backend/ && npm install && cd ../frontend && npm install`;
3. run the three server by opening three terminal instances by opening three shells and running on each of them:
    - `cd backend/ && npm run dev`;
    - `cd frontend/ && npm run dev`;
    - `node backend/logger.js`;
4. now you can test the project by navigating to the frontend server URL (by default [http://localhost:5173](http://localhost:5173));
5. when you want to stop the execution, you need to kill each of the three terminals.

## Credits (Group 22B)
- [Luca Padalino](mailto:luca.padalino@mail.polimi.it)
- [Giovanni Paolino](mailto:giovanni.paolino@mail.polimi.it)
- [Ilaria Paratici](mailto:ilaria.paratici@mail.polimi.it)
- [Alexandru Ionut Pascu](mailto:alexandruionut.pascu@mail.polimi.it)

We extend our heartfelt gratitude to Mathyas Giudici, our tutor, for his guidance and support throughout the development of this project.